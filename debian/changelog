libsyntax-highlight-perl-improved-perl (1.01-8) unstable; urgency=medium

  [ Debian Janitor ]
  * Wrap long lines in changelog entries: 1.01-7.
  * Bump debhelper from old 12 to 13.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sat, 19 Nov 2022 19:18:18 +0000

libsyntax-highlight-perl-improved-perl (1.01-7) unstable; urgency=medium

  [ Debian Janitor ]
  * Apply multi-arch hints. + libsyntax-highlight-perl-improved-perl: Add
    Multi-Arch: foreign.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Thu, 13 Oct 2022 20:45:34 +0100

libsyntax-highlight-perl-improved-perl (1.01-6) unstable; urgency=medium

  [ Damyan Ivanov ]
  * declare conformance with Policy 4.1.3 (no changes needed)

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ Debian Janitor ]
  * Bump debhelper from old 10 to 12.
  * Set debhelper-compat version in Build-Depends.

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Fri, 17 Jun 2022 11:17:27 +0100

libsyntax-highlight-perl-improved-perl (1.01-5.1) unstable; urgency=medium

  * Non maintainer upload by the Reproducible Builds team.
  * No source change upload to rebuild on buildd with .buildinfo files.

 -- Holger Levsen <holger@debian.org>  Tue, 05 Jan 2021 01:28:19 +0100

libsyntax-highlight-perl-improved-perl (1.01-5) unstable; urgency=medium

  * Team upload.
  * Fix syntactical typos in debian/copyright.
  * Fix typo in previous changelog entry.
  * Set "Rules-Requires-Root: no".
  * Add patch to remove syntactically incorrect =over/=back pair.
  * Add lintian override for application-in-library-section.

 -- Axel Beckert <abe@debian.org>  Wed, 15 Nov 2017 08:32:17 +0100

libsyntax-highlight-perl-improved-perl (1.01-4) unstable; urgency=medium

  * Team upload.

  [ gregor herrmann ]
  * Add debian/README.source to document quilt usage, as required by
    Debian Policy since 3.8.0.
  * debian/control: Changed: Switched Vcs-Browser field to ViewSVN
    (source stanza).
  * Change my email address.
  * debian/control: update {versioned,alternative} (build) dependencies.
  * Remove Jaldhar H. Vyas from Uploaders. Thanks for your work!

  [ Nathan Handler ]
  * debian/watch: Update to ignore development releases.

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs
  * Update Vcs-Browser URL to cgit web frontend
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ Axel Beckert ]
  * Convert to Debian source format "3.0 (quilt)".
  * Switch to a dh-v7-style debian/rules file and compat level 10.
    + Update versioned debhelper build-dependency accordingly.
    + Install files via debian/{install,docs,manpages}.
  * Declare compliance with Debian Policy 4.1.1.
  * Mention module name in long package description.
  * Convert debian/copyright to machine-readable DEP5 format.
  * Add DEP3 header to debian/patches/viewperl.patch.
  * Fix spelling errors found by Lintian.

 -- Axel Beckert <abe@debian.org>  Wed, 08 Nov 2017 02:16:49 +0100

libsyntax-highlight-perl-improved-perl (1.01-3) unstable; urgency=low

  * debian/control: Added: Vcs-Svn field (source stanza); Vcs-Browser
    field (source stanza); Homepage field (source stanza). Removed: XS-
    Vcs-Svn fields.
  * debian/rules:
    - delete /usr/lib/perl5 only if it exists (closes: #468018)
    - update based on dh-make-perl's templates
  * debian/watch: use dist-based URL.
  * Set Standards-Version to 3.7.3 (no changes).
  * debian/copyright: use generic download URL.

 -- gregor herrmann <gregor+debian@comodo.priv.at>  Wed, 12 Mar 2008 20:35:38 +0100

libsyntax-highlight-perl-improved-perl (1.01-2) unstable; urgency=low

  [ gregor herrmann ]
  * Add watch file.
  * Patch viewperl to make it use the actual library we're installing.
  * Add manpage for viewperl.

  [ Jaldhar H. Vyas ]
  * Added location of upstream tarball to debian/copyright.

 -- Jaldhar H. Vyas <jaldhar@debian.org>  Mon, 27 Aug 2007 02:12:33 -0400

libsyntax-highlight-perl-improved-perl (1.01-1) unstable; urgency=low

  * Initial Release.

 -- Jaldhar H. Vyas <jaldhar@debian.org>  Wed, 22 Aug 2007 14:36:43 -0400
